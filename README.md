# Infinitum SQL Database

A helper library written in C# to manage CRUD (Create, Read, Update, Delete) operations of any SQL related database.

Copyright (c) 2019 [INFINITUM](https://www.infinitum.lk). All rights reserved.