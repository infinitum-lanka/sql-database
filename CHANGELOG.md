# Change Log
All notable changes to this project will be documented in this file.

## [0.0.0.3] - 13-02-2018
- SQLServer Database Support Added.

## [0.0.0.2] - 10-02-2018
- SQLlite Database Support Added.

## [0.0.0.1] - 01-01-2018
- Created Global Database Interface.
- MySQL Database Support Added.
- Assembly Information Updated.
